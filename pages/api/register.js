import fs from "fs";

export default function handler(req, res) {
  if (req.method === "POST") {
    const { email, password } = req.body;
    // Ghi dữ liệu vào file
    fs.appendFileSync(
      "./pages/api/dataRegister.txt",
      `Email: ${email}, Password: ${password}\n`,
      "utf-8"
    );

    res.status(200).json({ message: "Register successful" });
  } else {
    res.status(405).json({ message: "Method not allowed" });
  }
}
