import { Typography } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { TextField } from "@mui/material";
import { Box } from "@mui/system";
import React from "react";
const useStyles = makeStyles({
  contactNumber: {
    marginBottom: "35px",
    display: "flex",
  },
  error: {
    color: "red",
    fontSize: "14px",
  },
});
const InputForm = ({ id, label, type, placeholder, register, error }) => {
  const classes = useStyles();
  return (
    <>
      <Box sx={{ width: 500, maxWidth: '100%', mb: 3 }}>
        <TextField
          fullWidth
          id={id}
          label={label}
          type={type}
          placeholder={placeholder}
          {...register}
        />
        <Typography className={classes.error}>{error}</Typography>
      </Box>
    </>
  );
};


export default InputForm;

