import React from "react";

const CountrySelection = ({ options, onChange, field }) => {
  const regionNamesInEnglish = new Intl.DisplayNames(['en'], { type: 'region' });
  return (
    <div style={{ display: "flex", flexDirection: "column", marginBottom: 25, maxWidth: "500px" }}>
      <label style={{ marginBottom: 10 }}>Select Country</label>
      <select
        style={{ padding: "16.5px 14px" }}
        defaultValue={options[3]?.code}
        onChange={(e) => onChange(e.target.value)}  {...field}
      >
        {options.map((option, index) => (
          <option key={`index${index}`} value={option.code} >
            {regionNamesInEnglish.of(option.code)}
          </option>
        ))}
      </select>
    </div>
  );
};

export default CountrySelection;
