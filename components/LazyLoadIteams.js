import { makeStyles } from "@material-ui/core/styles";
import { Container, Grid } from '@mui/material';
import React from 'react';

const useStyle_Skeleton = makeStyles({
  containerSkeleton: {
    gap: "36px",
    marginTop: "20px",
    width: "100%"
  },
  card: {
    width: "264px",
    height: "auto",
    background: "#CFCFCF",
    padding: "10px",
    borderRadius: "8px",
    "@media (max-width: 768px)": {
      width: "100%",
    }
  },
  cartSkeleton: {
    backgroundImage: "linear-gradient(90deg, #E8E8E8 0px, rgb(229 229 229 / 90%) 40px, #E8E8E8 80px)",
    backgroundSize: "300%",
    backgroundPosition: "100% 0 ",
    borderRadius: "inherit",
    animation: "$myEffect 1.5s infinite",
  },
  cart_title: {
    height: "35px",
    marginTop: "15px",
  },
  cart_description: {
    height: "250px"
  },
  "@keyframes myEffect": {
    "to": {
      backgroundPosition: "-100% 0"
    }
  }
});

function LazyLoadIteams() {
  const classes = useStyle_Skeleton();

  return (
    <div className={classes.containerSkeleton}>
      <div className={classes.card}>
        <div className={`${classes.cartSkeleton} ${classes.cart_description}`}></div>
        <div className={`${classes.cartSkeleton} ${classes.cart_title}`}></div>
      </div>
    </div>
  )
}

export default LazyLoadIteams
