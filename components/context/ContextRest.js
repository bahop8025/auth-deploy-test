import { createContext, useEffect, useState } from "react";
import CryptoJS from "crypto-js";

export const AppContextRest = createContext(null);

let initialCart;
try {
  initialCart = JSON.parse(localStorage.getItem("cart")) ?? [];
} catch {
  initialCart = [];
}
export const AppProviderRest = (props) => {
  const [cart, setCart] = useState(initialCart);
  const [error, setError] = useState(null);
  const [totalQuantity, setTotalQuantity] = useState(0);
  const [user, setUser] = useState(null);

  const secretKey = "8s1dH34c20p9c8s1dH34c20p9c8s1dH34c20p9c";

  useEffect(() => {
    const savedUser = localStorage.getItem("user");
    if (savedUser) {
      const decryptedBytes = CryptoJS.AES.decrypt(savedUser, secretKey);
      const decryptedData = decryptedBytes.toString(CryptoJS.enc.Utf8);
      setUser(JSON.parse(decryptedData));
    }
    // const savedUser = localStorage.getItem("user");
    // if (savedUser) {
    //   setUser(JSON.parse(savedUser));
    // }
  }, []);

  const loginUser = (userData) => {
    // // Mã hóa thông tin tài khoản
    const encryptedData = CryptoJS.AES.encrypt(
      JSON.stringify(userData),
      secretKey
    ).toString();
    localStorage.setItem("user", encryptedData);
    setUser(userData);

    // setUser(userData);
    // localStorage.setItem("user", JSON.stringify(userData));
  };

  const logoutUser = () => {
    setUser(null);
    localStorage.removeItem("user");
  };

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
    localStorage.setItem("totalQuantity", totalQuantity.toString());
  }, [cart, totalQuantity]);

  function emptyCart() {
    try {
      setCart([]);
      setTotalQuantity(0);
      localStorage.setItem("cart", "[]");
      localStorage.setItem("totalQuantity", "0");
    } catch (e) {
      setError(e);
    }
  }

  function clearCart() {
    setCart([]);
    setTotalQuantity(0);
    localStorage.setItem("cart", "[]");
    localStorage.setItem("totalQuantity", "0");
  }

  const muchAddToCart = (product, quantity) => {
    const existingItem = cart.find((item) => item.id === product.id);
    const updatedQuantity = existingItem
      ? existingItem.quantity + quantity
      : quantity;

    const updatedCartItems = existingItem
      ? cart.map((item) =>
          item.id === product.id ? { ...item, quantity: updatedQuantity } : item
        )
      : [...cart, { ...product, quantity: updatedQuantity }];

    setCart(updatedCartItems);
  };

  const updateQuantity = (productId, newQuantity) => {
    const updatedCartItems = cart.map((item) => {
      if (item.id === productId) {
        return { ...item, quantity: newQuantity };
      }
      return item;
    });
    setCart(updatedCartItems);
  };

  function removeProductFromCart(productId) {
    const updatedCartItems = cart.filter((item) => item.id !== productId);
    setCart(updatedCartItems);
  }

  useEffect(() => {
    const newTotalQuantity = cart.reduce((sum, item) => sum + item.quantity, 0);
    setTotalQuantity(newTotalQuantity);
  }, [cart]);

  function resetQuantity() {
    setTotalQuantity(0);
    localStorage.setItem("totalQuantity", "0");
  }

  const contextValue = {
    cart,
    setCart,
    emptyCart,
    removeProductFromCart,
    clearCart,
    error,
    updateQuantity,
    muchAddToCart,
    totalQuantity,
    resetQuantity,
    user,
    loginUser,
    logoutUser,
  };
  return (
    <AppContextRest.Provider value={contextValue}>
      {props.children}
    </AppContextRest.Provider>
  );
};
