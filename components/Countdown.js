import React, { useEffect, useState } from "react";
import { countdown } from "../utils/countdown";
import { makeStyles } from "@material-ui/core/styles";
const TIME = 42 * 24 * 60 * 60;

const useStyles = makeStyles({
  containerCount: {
    display: "flex !important",
    alignItems: "center",
  },
  circle: {
    width: 51,
    height: 51,
    borderRadius: 30,
    backgroundColor: "#eee",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    flexDirection: "column",
    marginRight: 15,
    padding: 30,
  },
  value: { margin: 0, fontSize: 20, fontWeight: "bold", color: "#666" },
  label: {
    margin: 0,
    fontSize: 12,
    color: "#444",
    fontWeight: "bold",
  },
});
export default function CountDown() {
  const classes = useStyles();
  const [time, setTime] = useState(TIME);
  const { days, hours, minutes, seconds } = countdown(time);

  useEffect(() => {
    const secTimer = setInterval(() => {
      setTime((prevState) => prevState - 1);
    }, 1000);
    return () => clearInterval(secTimer);
  }, []);

  return (
    <div className={classes.containerCount}>
      <span className={classes.circle}>
        <span className={classes.value}>{days}</span>
        <span className={classes.label}>days</span>
      </span>
      <span className={classes.circle}>
        <span className={classes.value}>{hours}</span>
        <span className={classes.label}>hours</span>
      </span>
      <span className={classes.circle}>
        <span className={classes.value}>{minutes}</span>
        <span className={classes.label}>mins</span>
      </span>
      <span className={classes.circle}>
        <span className={classes.value}>{seconds}</span>
        <span className={classes.label}>secs</span>
      </span>
    </div>
  );
}
