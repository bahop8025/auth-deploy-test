import { makeStyles } from "@material-ui/core/styles";
import { Link } from "@mui/material";
import Box from "@mui/material/Box";
import Image from "next/image";
import * as React from "react";

const useStyles = makeStyles({
  wrapperImage: {
    display: "flex",
    alignItems: "center",
    marginTop: "50px",

    "@media (max-width: 768px)": {
      display: "block",
      alignItems: "inherit",
    },
  },
  rowImage: {
    display: "flex",
    marginBottom: "20px",
    "@media (max-width: 600px)": {
      display: "block",
      marginBottom: "30px",
    },
  },
  box_1Image: {
    boxSizing: "border-box",
    paddingRight: "15px",
    "@media (max-width: 768px)": {
      paddingRight: "0px",
      marginBottom: "30px",
    },
  },
  box_Image: {
    marginBottom: "30px",
  },
  box_2Image: {
    display: "flex",
    paddingRight: "15px",
    paddingLeft: "15px",
    "@media (max-width: 768px)": {
      paddingRight: "0px",
      marginBottom: "30px",
      paddingLeft: "0px",
    },
    "@media (max-width: 600px)": {
      paddingLeft: "0px",
      display: "block",
    },
  },
  box_2Image1: {
    paddingRight: "15px",
    marginBottom: "30px",
    "@media (max-width: 768px)": {
      paddingRight: "0px",
      marginBottom: "30px",
    },
  },
  box_2Image2: {
    paddingRight: "30px",
    "@media (max-width: 600px)": {
      paddingRight: "0px",
      marginBottom: "30px",
    },
  },
  box_3Image: {
    paddingRight: "15px",
    paddingLeft: "15px",
    "@media (max-width: 768px)": {
      width: "100%",
      paddingLeft: "0px",
      marginBottom: "30px",
    },
    "@media (max-width: 600px)": {
      paddingRight: "0px",
    },
  },
  box_4Image: {
    paddingLeft: "15px",
    "@media (max-width: 768px)": {
      width: "100%",
    },
    "@media (max-width: 600px)": {
      paddingLeft: "0px",
    },
  },
  Image: {
    width: "100%",
    transition: "all .5s",
    "@media (max-width: 768px)": {
      width: "100%",
    },
  },
});

export default function NewImageList() {
  const classes = useStyles();
  return (
    <>
      <Box className={classes.wrapperImage}>
        <Box className={classes.box_1Image}>
          <Box>
            <Link href="/shop">
              <a>
                <Image
                  width={449}
                  height={519}
                  src="/seller_1.png"
                  alt="image1"
                  className={classes.Image}
                />
              </a>
            </Link>
          </Box>
        </Box>
        <Box className={classes.rowImage}>
          <Box>
            <Box className={classes.box_2Image}>
              <Box className={classes.box_2Image1}>
                <Link href="/product/pny-micro-hc">
                  <a>
                    <Image
                      width={462}
                      height={252}
                      src="/seller_2.png"
                      alt="image2"
                      className={classes.Image}
                    />
                  </a>
                </Link>
              </Box>
              <Box>
                <Link href="/product/dz09-smart-watch">
                  <a>
                    <Image
                      width={307}
                      height={259}
                      src="/seller_3.png"
                      alt="image3"
                      className={classes.Image}
                    />
                  </a>
                </Link>
              </Box>
            </Box>
            <Box className={classes.box_2Image}>
              <Box className={classes.box_2Image2}>
                <Link href="/product/fastfood-humburger-with-option">
                  <a>
                    <Image
                      width={295}
                      height={250}
                      src="/seller_6.png"
                      alt="image6"
                      className={classes.Image}
                    />
                  </a>
                </Link>
              </Box>
              <Box>
                <Link href="/product/black-skirt-for-women/">
                  <a>
                    <Image
                      width={450}
                      height={245}
                      src="/seller_7.png"
                      alt="image7"
                      className={classes.Image}
                    />
                  </a>
                </Link>
              </Box>
            </Box>
          </Box>
        </Box>

        <Box className={classes.rowImage}>
          <Box className={classes.box_3Image}>
            <Box className={classes.box_Image}>
              <Link href="  /product/apple-swatch/">
                <a>
                  <Image
                    width={289}
                    height={244}
                    src="/seller_5.png"
                    alt="image5"
                    className={classes.Image}
                  />
                </a>
              </Link>
            </Box>
            <Box>
              <Link href=" /product/apple-swatch/">
                <a>
                  <Image
                    width={289}
                    height={244}
                    src="/seller_4.png"
                    alt="image4"
                    className={classes.Image}
                  />
                </a>
              </Link>
            </Box>
          </Box>
          <Box className={classes.box_4Image}>
            <Box className={classes.box_Image}>
              <Link href=" /product/baby-hand-bell-rattlle/">
                <a>
                  <Image
                    width={289}
                    height={244}
                    src="/seller_8.png"
                    alt="image8"
                    className={classes.Image}
                  />
                </a>
              </Link>
            </Box>
            <Box>
              <Link href="/product/american-humburger-with-salad/">
                <a>
                  <Image
                    width={289}
                    height={244}
                    src="/seller_9.png"
                    alt="image9"
                    className={classes.Image}
                  />
                </a>
              </Link>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
}
