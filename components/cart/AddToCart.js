import { Button } from '@mui/material';
import React, { useContext } from 'react';
import { AppContextRest } from '../context/ContextRest';
import { toast } from 'react-toastify';

function AddToCart({ product, quantity }) {
  const { muchAddToCart } = useContext(AppContextRest)

  const addToCart = (e) => {
    e.preventDefault()
    muchAddToCart(product, quantity)
    toast.success("Product added to cart!");
  }

  return (
    <>
      <form onSubmit={addToCart}>
        <Button type="submit" fullWidth variant="outlined" style={{ marginTop: 30 }}>Add to cart</Button>
      </form>
    </>
  )
}

export default AddToCart
