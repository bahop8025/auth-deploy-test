import DeleteForeverIcon from "@mui/icons-material/DeleteForever";
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";
import Typography from "@mui/material/Typography";
import { useContext, useState } from "react";
import { AppContextRest } from "../../context/ContextRest";
import { Button, Link } from "@material-ui/core";
import Image from "next/image";
import { makeStyles } from "@material-ui/core/styles";

const useStyles_CartItem = makeStyles((theme) => ({
  "@media (max-width: 768px)": {
    tdTable: {
      padding: "15px 0 !important",
    },
    imagesItem: {
      "& > span": {
        width: "50px !important",
        height: "50px !important",
      },
    },
    cellQuantity: {
      display: "flex",
      flexDirection: "column",
      margin: "10px 30px",
    },
  },
}));

const CartItem = ({ product }) => {
  const classes = useStyles_CartItem();
  const { updateQuantity, removeProductFromCart } = useContext(AppContextRest);
  const [quantity, setQuantity] = useState(product?.quantity);

  const handleIncreaseQuantity = () => {
    const newQuantity = Math.max(quantity + 1, 1);
    setQuantity(newQuantity);
    updateQuantity(product.id, newQuantity);
  };

  const handleDecreaseQuantity = () => {
    if (quantity > 0) {
      const newQuantity = Math.max(quantity - 1, 1);
      setQuantity(newQuantity);
      updateQuantity(product.id, newQuantity);
    }
  };

  return (
    <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
      <TableCell align="center" className={classes.tdTable}>
        <b
          onClick={() => {
            removeProductFromCart(product.id);
          }}
        >
          <DeleteForeverIcon />
        </b>
      </TableCell>
      <TableCell align="center" className={classes.tdTable}>
        <Link href={`/product/${product?.slug}`}>
          <a>
            <Image
              className={classes.imagesItem}
              width={150}
              height={150}
              src={product?.images?.[0].src}
              alt="Card image cap"
            />
          </a>
        </Link>
      </TableCell>
      <TableCell align="center" className={classes.tdTable}>
        <Typography>
          <Link
            href={`/product/${product?.slug}`}
            style={{ textDecoration: "none" }}
          >
            <a>{product?.name}</a>
          </Link>
        </Typography>
      </TableCell>
      <TableCell
        align="center"
        className={`${classes.tdTable} ${classes.cellQuantity}`}
      >
        <Button variant="outlined" onClick={handleDecreaseQuantity}>
          -
        </Button>
        <span style={{ margin: "0 5px 0 5px" }}>{quantity}</span>
        <Button variant="outlined" onClick={handleIncreaseQuantity}>
          +
        </Button>
      </TableCell>
      <TableCell align="center" className={classes.tdTable}>
        <Typography>x</Typography>
      </TableCell>
      <TableCell align="center" className={classes.tdTable}>
        <Typography>${product?.price * product.quantity}</Typography>
      </TableCell>
    </TableRow>
  );
};

export default CartItem;
