import { Button, Typography } from "@material-ui/core";
import {
  FormControl,
  FormControlLabel,
  Radio,
  RadioGroup,
} from "@mui/material";
import ListItem from "@mui/material/ListItem";
import React, { useEffect, useState } from "react";
import SendIcon from "@mui/icons-material/Send";
import { useRouter } from "next/router";

function Shipping({ isResult }) {
  const router = useRouter();
  const [selectedOption, setSelectedOption] = useState(null);
  const [isShowButtonCheckOut, setIsShowButtonCheckOut] = useState(false);
  const handleOpen = () => router.push("/checkout");

  const handleOptionClick = (option) => {
    setSelectedOption(option);
    if (typeof window !== "undefined") {
      window.localStorage.setItem("selectedOption", parseInt(option));
    }
  };

  useEffect(() => {
    setIsShowButtonCheckOut(true);
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();
    if (selectedOption) {
      console.log("Selected Option:", selectedOption);
      handleOpen();
    } else {
      console.log("Please select a shipping method.");
    }
  };

  return (
    <>
      <Typography
        style={{ margin: "10px 0 5px 16px" }}
        component="h5"
        variant="h5"
      >
        Shipping Method
      </Typography>
      <ListItem style={{ marginLeft: "10px" }}>
        <FormControl>
          <form onSubmit={handleSubmit}>
            <RadioGroup name="options">
              {isResult &&
                isResult.map((option) => {
                  const resultOption = option.settings?.cost?.value ?? 0;
                  return (
                    <FormControlLabel
                      style={{ marginBottom: "5px" }}
                      key={option.id}
                      value={option?.id}
                      control={<Radio />}
                      label={option.method_title}
                      onClick={() => handleOptionClick(resultOption)}
                    />
                  );
                })}
            </RadioGroup>
            {isShowButtonCheckOut && (
              <Button
                variant="outlined"
                endIcon={<SendIcon />}
                onClick={handleOpen}
                type="submit"
                disabled={selectedOption === null}
              >
                Proceeed to checkout
              </Button>
            )}
          </form>
        </FormControl>
      </ListItem>
    </>
  );
}

export default Shipping;
