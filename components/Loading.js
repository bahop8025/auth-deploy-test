import React from "react";
import { makeStyles } from "@material-ui/core/styles";
const useStyle_loading = makeStyles({
  overlay: {
    position: "fixed",
    top: 0,
    left: 0,
    width: "100%",
    height: "100%",
    background: "rgba(0, 0,0,0.3)",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    zIndex: 100,
    // pointerEvents: "none",
  },
  spinner: {
    border: "4px solid rgba(255,255,255, 1)",
    borderLeftColor: "transparent",
    borderRadius: "50%",
    width: "46px",
    height: "46px",
    animation: "$spin 1s linear infinite",
    zIndex: 200,
  },
  "@keyframes spin": {
    "0%": {
      transform: "rotate(0deg)",
    },
    "100%": {
      transform: "rotate(360deg)",
    },
  },
});

const Loading = () => {
  const classes = useStyle_loading();
  return (
    <div className={classes.overlay}>
      <div className={classes.spinner}></div>
    </div>
  );
};

export default Loading;
