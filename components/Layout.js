import React, { useEffect, useState } from "react";
import AddHead from "./AddHead";
import Copyright from "./Copyright";
import Footer from "./Footer";
import HeaderCenter from "./HeaderCenter";
import HeaderMid from "./HeaderMid";
import { AppProviderRest } from "./context/ContextRest";
import SideBarTop from "./sidebar";
import { ToastContainer } from "react-toastify";
import { useRouter } from "next/router";
import Loading from "./Loading";

const Layout = (props) => {
  const router = useRouter();
  const { categorized } = props;

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    const handleStart = () => {
      setIsLoading(true);
    };

    const handleComplete = () => {
      setIsLoading(false);
    };

    router.events.on("routeChangeStart", handleStart);
    router.events.on("routeChangeComplete", handleComplete);
    router.events.on("routeChangeError", handleComplete);

    return () => {
      router.events.off("routeChangeStart", handleStart);
      router.events.off("routeChangeComplete", handleComplete);
      router.events.off("routeChangeError", handleComplete);
    };
  }, []);

  return (
    <AppProviderRest>
      <div>
        {isLoading && <Loading />}
        <AddHead />
        <SideBarTop />
        <HeaderMid categorized={categorized} />
        <HeaderCenter categorized={categorized} />
        {props.children}
        <Footer />
        <Copyright />
      </div>
      <ToastContainer
        position="bottom-right"
        autoClose={3000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="light"
      />
    </AppProviderRest>
  );
};
export default Layout;
