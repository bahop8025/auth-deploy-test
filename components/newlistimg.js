import { makeStyles } from "@material-ui/core/styles";
import { Link } from "@mui/material";
import Box from "@mui/material/Box";
import Image from "next/image";
import * as React from "react";

const useStyles = makeStyles({
  wrapperImage: {
    display: "flex",
    marginTop: "50px",

    "@media (max-width: 768px)": {
      display: "block",
    },
  },
  rowImage: {
    display: "flex",
    marginBottom: "20px",
    "@media (max-width: 600px)": {
      display: "block",
      marginBottom: "30px",
    },
  },
  box_1Image: {
    boxSizing: "border-box",
    paddingRight: "15px",
    "@media (max-width: 768px)": {
      paddingRight: "0px",
      marginBottom: "30px",
    },
  },
  box_Image: {
    marginBottom: "30px",
  },
  box_2Image: {
    paddingRight: "15px",
    paddingLeft: "15px",
    "@media (max-width: 768px)": {
      paddingRight: "0px",
      marginBottom: "30px",
    },
    "@media (max-width: 600px)": {
      paddingLeft: "0px",
    },
  },
  box_3Image: {
    paddingRight: "15px",
    paddingLeft: "15px",
    "@media (max-width: 768px)": {
      width: "100%",
      paddingLeft: "0px",
      marginBottom: "30px",
    },
    "@media (max-width: 600px)": {
      paddingRight: "0px",
    },
  },
  box_4Image: {
    paddingLeft: "15px",
    "@media (max-width: 768px)": {
      width: "100%",
    },
    "@media (max-width: 600px)": {
      paddingLeft: "0px",
    },
  },
  Image: {
    width: "100%",
    transition: "all .5s",
    "@media (max-width: 768px)": {
      width: "100%",
    },
  },
});

export default function NewImageList() {
  const classes = useStyles();
  return (
    <>
      <Box className={classes.wrapperImage}>
        <Box className={classes.rowImage}>
          <Box className={classes.box_1Image}>
            <Box className={classes.box_Image}>
              <Link href="/product/apple-iphone-7-plus">
                <Image
                  width={610}
                  height={290}
                  src="/image_1.png"
                  alt="image1"
                  className={classes.Image}
                />
              </Link>
            </Box>
            <Box>
              <Link href="/product/ring-with-12-round-diamond/">
                <Image
                  width={610}
                  height={290}
                  src="/image_2.png"
                  alt="image2"
                  className={classes.Image}
                />
              </Link>
            </Box>
          </Box>
          <Box className={classes.box_2Image}>
            <Box>
              <Link href=" /shop">
                <Image
                  width={615}
                  height={615}
                  src="/image_3.png"
                  alt="image3"
                  className={classes.Image}
                />
              </Link>
            </Box>
          </Box>
        </Box>

        <Box className={classes.rowImage}>
          <Box className={classes.box_3Image}>
            <Box className={classes.box_Image}>
              <Link href="/product/lenovo-15-e460-notebook">
                <Image
                  width={293}
                  height={293}
                  src="/image_4.png"
                  alt="image4"
                  className={classes.Image}
                />
              </Link>
            </Box>
            <Box>
              <Link href="/product/wireless-mouse-m187">
                <Image
                  width={293}
                  height={293}
                  src="/image_5.png"
                  alt="image5"
                  className={classes.Image}
                />
              </Link>
            </Box>
          </Box>
          <Box className={classes.box_4Image}>
            <Box className={classes.box_Image}>
              <Link href="/product/dz09-smart-watch">
                <Image
                  width={293}
                  height={293}
                  src="/image_6.png"
                  alt="image6"
                  className={classes.Image}
                />
              </Link>
            </Box>
            <Box>
              <Link href="/product/lum-5d-mark-iv-dslr-camera">
                <Image
                  width={293}
                  height={293}
                  src="/image_7.png"
                  alt="image7"
                  className={classes.Image}
                />
              </Link>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
}
