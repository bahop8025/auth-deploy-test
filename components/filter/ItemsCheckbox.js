import React from 'react';
import FilterCheckboxGroup from './FilterCheckboxGroup';

export function FilterResolution({ onRadioChange, resolution, attributes }) {
  return (
    <FilterCheckboxGroup
      onRadioChange={onRadioChange}
      attributes={attributes}
      items={resolution}
    />
  );
}

export function FilterInputsOutputs({ onRadioChange, attributes, inOut }) {
  return (
    <FilterCheckboxGroup
      onRadioChange={onRadioChange}
      attributes={attributes}
      items={inOut}
    />
  );
}

export function FilterBy({ onRadioChange, attributes, filterBy }) {
  return (
    <FilterCheckboxGroup
      onRadioChange={onRadioChange}
      attributes={attributes}
      items={filterBy}
    />
  );
}

export function FilterTvType({ onRadioChange, attributes, TvTypeTerm }) {
  return (
    <FilterCheckboxGroup
      onRadioChange={onRadioChange}
      attributes={attributes}
      items={TvTypeTerm}
    />
  );
}

export function FilterTVSize({ onRadioChange, attributes, size }) {
  return (
    <FilterCheckboxGroup
      onRadioChange={onRadioChange}
      attributes={attributes}
      items={size}
    />
  );
}

