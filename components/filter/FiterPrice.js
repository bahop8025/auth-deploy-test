import React from 'react'
import Slider from '@mui/material/Slider';
import { useState } from 'react';


function valuetext(value) {
  return `${value}`;
}

const minDistance = 10;

function FiterPrice({ onValueChange }) {

  const [value, setValue] = useState([0, 100]);

  const handleChangeFilterPrice = (event, newValue, activeThumb) => {

    onValueChange(newValue);

    if (!Array.isArray(newValue)) {
      return;
    }

    let updatedValue;
    if (activeThumb === 0) {
      updatedValue = [Math.min(newValue[0], value[1] - minDistance), value[1]];
    } else {
      updatedValue = [value[0], Math.max(newValue[1], value[0] + minDistance)];
    }

    setValue(updatedValue);
  };

  return (
    <>
      <Slider
        value={value}
        onChange={handleChangeFilterPrice}
        valueLabelDisplay="auto"
        getAriaValueText={valuetext}
        disableSwap
      />
    </>
  )
}
export default FiterPrice
