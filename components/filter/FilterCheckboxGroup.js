import { Checkbox, FormGroup } from '@material-ui/core';
import FormControlLabel from '@mui/material/FormControlLabel';
import React, { useState } from 'react';

function FilterCheckboxGroup({ onRadioChange, items }) {
  const [selectedItemId, setSelectedItemId] = useState(null);

  const handleCheckboxChange = (id, slug_parent) => () => {
    if (selectedItemId === id) {
      // Bỏ chọn nếu đã chọn rồi
      setSelectedItemId(null);
      onRadioChange(null, null, false);
    } else {
      // Chọn mục mới và hủy chọn mục cũ (nếu có)
      setSelectedItemId(id);
      onRadioChange(id, slug_parent, true);
    }
  };

  return (
    <div>
      <FormGroup>
        {items.map(item => (
          <FormControlLabel
            key={item.id}
            control={
              <Checkbox
                checked={selectedItemId === item.id}
                onChange={handleCheckboxChange(item.id, item.slug_parent)}
              />
            }
            label={item.name}
          />
        ))}
      </FormGroup>
    </div>
  );
}

export default FilterCheckboxGroup;
