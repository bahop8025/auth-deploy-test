import { makeStyles } from "@material-ui/core/styles";
import { useRouter } from "next/router";
import React, { useState } from "react";
import { useEffect } from "react";
const filterColor_Style = makeStyles((theme) => ({
  styleColor: {
    padding: "0",
  },
  colorOption: {
    display: "inline-block",
    width: "30px",
    height: "30px",
    borderRadius: "50%",
    marginRight: "5px",
    border: "1px solid #000",
    cursor: "pointer",
  },
  inputCheck: {
    opacity: "0",
    pointerEvents: "none",
    width: "0",
    height: "0",
  },
  activeColor: {
    border: "3px solid #000",
    display: "inline-block",
    width: "35px",
    height: "35px",
    borderRadius: "50%",
    marginRight: "5px",
    cursor: "pointer",
  },
}));

function FilterColor({ colorProduct, onRadioChange }) {
  const classes = filterColor_Style();
  const [checkedItems, setCheckedItems] = useState([]);
  const router = useRouter();
  const { query } = router;
  const [colorIdFromUrl, setColorIdFromUrl] = useState(query.id);

  useEffect(() => {
    if (checkedItems.includes(colorIdFromUrl)) {
      return;
    }
    if (checkedItems.length > 0) {
      setColorIdFromUrl(checkedItems[0]);
    } else {
      setColorIdFromUrl(null);
    }
  }, [checkedItems, colorIdFromUrl]);

  const handleCheckboxChange = (id, slug_parent) => (event) => {
    const isChecked = event.target.checked;
    if (isChecked) {
      setCheckedItems([...checkedItems, id]);
    } else {
      setCheckedItems(checkedItems.filter((item) => item !== id));
    }
    setColorIdFromUrl(id);
    onRadioChange(id, slug_parent, isChecked);
  };

  return (
    <>
      <ul className={classes.styleColor}>
        {colorProduct &&
          colorProduct.map((item) => {
            return (
              <label key={item.id}>
                <input
                  type="checkbox"
                  value={item.slug}
                  checked={checkedItems.includes(item.id)}
                  onChange={handleCheckboxChange(item.id, item.slug_parent)}
                  className={`${classes.inputCheck}`}
                />
                <span
                  className={`${classes.colorOption} ${
                    item.id === colorIdFromUrl ? classes.activeColor : ""
                  }`}
                  style={{ backgroundColor: item.slug }}
                ></span>
              </label>
            );
          })}
      </ul>
    </>
  );
}

export default FilterColor;
