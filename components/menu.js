import { makeStyles } from "@material-ui/core/styles";
import ListIcon from "@mui/icons-material/List";
import Box from "@mui/material/Box";
import ClickAwayListener from "@mui/material/ClickAwayListener";
import Grow from "@mui/material/Grow";
import ListItemIcon from "@mui/material/ListItemIcon";
import MenuList from "@mui/material/MenuList";
import Paper from "@mui/material/Paper";
import Popper from "@mui/material/Popper";
import Link from "next/link";
import * as React from "react";
import { useEffect, useRef, useState } from "react";
const colorHover = "#40c6ff";
const useStyle_category_header = makeStyles({
  buttonCategory: {
    width: "100% !important",
    color: "#444",
    fontSize: "14px",
    fontWeight: "bold",
    textTransform: "uppercase",
    display: "flex",
    justifyContent: "left",
    alignItems: "center",
    height: "43px",
    paddingRight: "15px",
    border: "0px",
    background: "#fff",
    borderTopLeftRadius: "2px",
    borderTopRightRadius: "2px",
    minWidth: "270px",
    marginTop: 13,
    "@media (max-width: 768px) ": {
      marginTop: "0px",
    },
    "&.active": {
      cursor: "pointer",
      background: `${colorHover}`,
      color: "#fff",
      "& svg": {
        marginLeft: "10px",
        transition: "all 0.25s",
        color: "#fff",
      },
    },
    "& svg": {
      marginLeft: "-5px",
      fontSize: "30px",
      color: "#444",
      transition: "all 0.25s",
    },
    '&:aria-expanded="true"': {
      cursor: "pointer",
      background: `${colorHover}`,
      color: "#fff",
      "& svg": {
        marginLeft: "10px",
        transition: "all 0.25s",
        color: "#fff",
      },
    },
    "&:hover": {
      cursor: "pointer",
      background: `${colorHover}`,
      color: "#fff",
      "& svg": {
        marginLeft: "10px",
        transition: "all 0.25s",
        color: "#fff",
      },
    },
  },
  listCategoryHome: {
    minWidth: "270px",
    zIndex: "100",
    "& li": {
      fontSize: "14px",
      textTransform: "capitalize",
      lineHeight: " 45px",
      height: "45px",
      "& a": {
        color: "#444",
      },
      "& svg": {
        fontSize: "14px",
      },
      "&:hover": {
        backgroundColor: "rgba(64,198,255,0.4)",
        "& svg": {
          color: "#fff",
        },
      },
    },
  },
  Category: {
    zIndex: 10,
  },
  menu: {
    position: "relative",
  },
  subMenu: {
    position: "relative",
    top: "-45px",
    display: "none",
    // position: "absolute",
    left: "100%",
    background: "#fff",
    borderRadius: "5px",
    width: "100%",
    "@media (max-width: 768px)": {
      position: "sticky",
      top: 0,
      borderRadius: 0,
      background: "fff",
      boxShadow:
        "rgba(9, 30, 66, 0.25) 0px 4px 8px -2px, rgba(9, 30, 66, 0.08) 0px 0px 0px 1px",
    },
  },
  menu1: {
    cursor: "pointer",
    padding: "0 5px",
    display: "flex",
    flexDirection: "column",
    marginBottom: "0",
    transition: "margin-bottom 0.3s",
  },
  menu1: {
    "&:hover $subMenu": {
      display: "block",
    },
  },
  listItem: {
    padding: "0 10px",
  },
  item: {
    fontSize: "16px !important",
    padding: "0 5px !important",
  },
});

export default function MenuListComposition({ categorized }) {
  const classes = useStyle_category_header();
  const [open, setOpen] = useState(false);
  const anchorRef = useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  function handleListKeyDown(event) {
    if (event.key === "Tab") {
      event.preventDefault();
      setOpen(false);
    } else if (event.key === "Escape") {
      setOpen(false);
    }
  }

  const prevOpen = useRef(open);
  useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);

  return (
    <Box className={`header`}>
      <button
        className={
          open === false
            ? `${classes.buttonCategory}`
            : `${classes.buttonCategory}` + " " + `active`
        }
        ref={anchorRef}
        id="composition-button"
        aria-controls={open ? "composition-menu" : undefined}
        aria-expanded={open ? "true" : undefined}
        aria-haspopup="true"
        onClick={handleToggle}
      >
        <ListItemIcon>
          <ListIcon />
        </ListItemIcon>
        <p>ALL CATEGORY</p>
      </button>
      <Popper
        className={classes.Category}
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        placement="bottom-start"
        transition
        disablePortal
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin:
                placement === "bottom-start" ? "left top" : "left bottom",
            }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <div className={classes.menu}>
                  <ul className={classes.listItem}>
                    <MenuList
                      className={classes.listCategoryHome}
                      autoFocusItem={open}
                      id="composition-menu"
                      aria-labelledby="composition-button"
                      onKeyDown={handleListKeyDown}
                    >
                      {categorized.map((item) => {
                        return (
                          <li className={classes.menu1} key={item.id}>
                            <Link href={`/shop?category=${item.id}`}>
                              <a className={item}>{item.name}</a>
                            </Link>
                            <ul className={classes.subMenu}>
                              {item.children &&
                                item.children.map((subItem) => (
                                  <li key={subItem.id}>
                                    <Link href={`/shop?category=${subItem.id}`}>
                                      <a>{subItem.name}</a>
                                    </Link>
                                  </li>
                                ))}
                            </ul>
                          </li>
                        );
                      })}
                    </MenuList>
                  </ul>
                </div>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </Box>
  );
}
