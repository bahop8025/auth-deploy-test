import WooCommerce from "./apiWoocommerce";
// export const getChildCategories = async (parentId) => {
//   try {
//     const res = await WooCommerce.get("products/categories", {
//       parent: parentId,
//     });
//     const childCategories = res.data;
//     return childCategories;
//   } catch (error) {
//     console.error("Error in getChildCategories:", error);
//     throw error;
//   }
// };

export const getCategoriesData = async (
  // perPage = 100,
  page = 1
  // parentId = 0
) => {
  try {
    const res = await WooCommerce.get("products/categories", {
      per_page: 100,
      page: page,
    });
    const categories = res.data;

    let array = [];
    categories.forEach((cate) => {
      if (cate.parent == 0) {
        let childCategories = categories.filter(
          (child) => child.parent == cate.id
        );
        var c = cate;
        c.children = childCategories;
        array.push(c);
      }
    });
    return {
      categories: array,
      totalProducts: res.headers["x-wp-total"],
      totalPages: res.headers["x-wp-totalpages"],
    };
  } catch (error) {
    console.error("Error in getCategoriesData:", error);
    throw error;
  }
};
