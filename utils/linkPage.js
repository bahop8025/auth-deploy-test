import FacebookSharpIcon from "@mui/icons-material/FacebookSharp";
import GoogleIcon from "@mui/icons-material/Google";
import HomeIcon from "@mui/icons-material/Home";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import LocalPhoneIcon from "@mui/icons-material/LocalPhone";
import MailIcon from "@mui/icons-material/Mail";
import PublicIcon from "@mui/icons-material/Public";
import TwitterIcon from "@mui/icons-material/Twitter";


export const linksSocialNetwork = [
  { href: process.env.LINK_FB, icon: <FacebookSharpIcon /> },
  { href: process.env.LINK_TWITTER, icon: <TwitterIcon /> },
  { href: process.env.LINK_LINKEDIN, icon: <LinkedInIcon /> },
  { href: process.env.LINK_GOOGLE, icon: <GoogleIcon /> },
]


export const linksContact = [
  { href: '', icon: <HomeIcon />, text: " PO Box CT16122 Collins Street West, Victoria 8007, Australia." },
  { href: "tel:+1 (2) 345 6789", icon: <LocalPhoneIcon />, text: "tel:+1 (2) 345 6789" },
  { href: "mailto:contact@yourdomain.com", icon: <MailIcon />, text: "contact@yourdomain.com" },
  { href: process.env.LINK_DOMAIN, icon: <PublicIcon />, text: "http://yourdomain.com" },
];

export const linksPageContactPhone = { href: 'tel:0-800-12-345-6 880', text: "0-800-12-345-6 880" }
export const linksPageContactLocation = { href: process.env.LINK_LOCATION_CONTACT, text: " 257 Charlington Gates Road Morrison, Nr. 568" }
export const linksPageContactEmail = { href: "mailto:info@jruitstemplate.com", text: "info@jruitstemplate.com" }