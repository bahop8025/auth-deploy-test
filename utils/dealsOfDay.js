import WooCommerce from "./apiWoocommerce";

export const getDealsData = async () => {
  const params = {
    category: 41,
    limit: 10,
    orderby: "date",
    order: "desc",
  };

  try {
    const res = await WooCommerce.get("products", params);

    const deals = res.data;
    return {
      deals: deals,
    };
  } catch (error) {
    console.error("Error in getDealsData:", error);
    throw error;
  }
};
