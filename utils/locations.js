import WooCommerce from "./apiWoocommerce";

export const getLocationsData = async () => {
  try {
    const res = await WooCommerce.get("shipping/zones/1/locations");
    const locations = res.data;
    return {
      locations: locations,
    };
  } catch (error) {
    console.error("Error in getLocationsData:", error);
    throw error;
  }
};
