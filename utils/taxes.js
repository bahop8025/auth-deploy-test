import WooCommerce from "./apiWoocommerce";

export const getTaxesData = async () => {
  try {
    const res = await WooCommerce.get("taxes");
    const taxes = res.data;
    return {
      taxes: taxes,
    };
  } catch (error) {
    console.error("Error in getTaxesData:", error);
    throw error;
  }
};
