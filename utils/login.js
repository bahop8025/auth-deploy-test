export async function getLogin() {
  try {
    const response = await fetch("https://jsonplaceholder.typicode.com/users/");
    const usersLogin = await response.json();
    return usersLogin;
  } catch (error) {
    console.error("Error in getLogin:", error);
    throw error;
  }
}
