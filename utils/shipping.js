import WooCommerce from "./apiWoocommerce";

export const getShippingData = async () => {
  try {
    const res = await WooCommerce.get("shipping/zones/1/methods");

    const shipping = res.data;
    return {
      shipping: shipping,
    };
  } catch (error) {
    console.error("Error in getShippingData:", error);
    throw error;
  }
};
