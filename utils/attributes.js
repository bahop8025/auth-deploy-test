import { memoize } from "lodash";
import WooCommerce from "./apiWoocommerce";

export const getAttributes = async () => {
  try {
    const res = await WooCommerce.get("products/attributes");
    const attributes = res.data;

    return {
      attributes: attributes,
    };
  } catch (error) {
    console.error("Error in getAttributes:", error);
    throw error;
  }
};


const getAttributesAll = async () => {
  try {
    const { attributes } = await getAttributes();

    const attributeResponses = await Promise.all(
      attributes.map(async (attribute) => {
        const { id, slug } = attribute;
        const res = await WooCommerce.get(`products/attributes/${id}/terms`);
        const attributeTerms = res.data.map((term) => ({
          ...term,
          parent_id: id,
          slug_parent: slug,
        }));
        return [id, attributeTerms];
      })
    );

    const attributesTerm = Object.fromEntries(attributeResponses);

    return {
      attributesAll: attributesTerm,
    };
  } catch (error) {
    console.error("Error in getAttributesAll:", error);
    throw error;
  }
};

export const memoizedGetAttributesAll = memoize(getAttributesAll);
